FROM node:alpine
WORKDIR /app
COPY package.json yarn.lock . /
RUN yarn --frozen-lockfile --production
COPY dist dist

CMD ["node", "dist/index.js"]
